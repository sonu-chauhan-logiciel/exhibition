<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Painting;

class Exhibition extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'name', 'start_datetime', 'end_datetime', 'address', 'city', 'state', 'status',
	];

	/**
	 * Scope a query to search users.
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	*/
	public function scopeFilter($query, $value)
	{
	    $query->where('name', 'LIKE', "%$value%");
	}
	
    /**
     * Get the Paintings.
     */
    public function paintings()
    {
        return $this->hasMany('App\Painting');
    }
}
