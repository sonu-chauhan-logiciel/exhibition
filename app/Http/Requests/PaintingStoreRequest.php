<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaintingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
        {
            if ($this->getMethod() == 'PUT') 
            {
                return [
                    'name' => 'required|string|max:255',
                ];        
            }
            else
            {
                return [
                    'exhibition' => 'required|min:1',
                    'name' => 'required|string|max:255',
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'status' =>'accepted'
                ];
            }
        }

        public function messages()
        {
            return [
                'exhibition' => 'Exhibition Field is Required',
                'name.required' => 'Name Field is Required!',
                'image.required' => 'Image Field is Required',
                'status.accepted' => 'Status Field is Required',
            ];
        }
}
