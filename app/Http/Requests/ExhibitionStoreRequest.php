<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExhibitionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->getMethod() == 'PUT')
        {
            return [
                'name' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state' => 'required',
            ];
        }else{
            return [
                'name' => 'required',
                'start_at' => 'required|date|date_format:Y-m-d|before:end_at',
                'end_at' => 'required|date|date_format:Y-m-d|after:start_at',
                'address' => 'required',
                'city' => 'required',
                'state' => 'required',
                'status' => 'accepted',    
            ];           
        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Name Field is required!',
            'start_at.required' => 'Start date Field is required',
            'end_at.required' => 'End date Field is required',
            'address.required' => 'Address Field id Required',
            'city.required' => 'City Field id Required',
            'state.required' => 'State Field id Required',
            'status.accepted' => 'Status Field id Required',
        ];
    }
}
