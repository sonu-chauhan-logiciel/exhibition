<?php

namespace App\Http\Controllers\Admin\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
Use App\User;

class LoginController extends Controller
{
	/**
	* Show the form for login User.
	*
	* @return \Illuminate\Http\Response
	*/
    public function showForm()
    {
    	return view('admin/login');
    }

    /**
     * Make User Login Request.
     *@param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
    	if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $request->session()->put('email', $email);
            $user = User::where('email', $email)->first();
            if($user->role_id == 1){
                return redirect()->route('admindashboard');
            }
            return redirect()->route('userdashboard');
        }
        return redirect()->route('login')->with('message', 'Invalid Credentials');
    }

    /**
     * User Logout.
     *@param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->flash('email');
        return redirect('/login');
    }
}
