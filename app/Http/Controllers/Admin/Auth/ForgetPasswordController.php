<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;

class ForgetPasswordController extends Controller
{
    /*
    * Show Form For Send Password Link
     */
    public function showForm()
    {
    	return view('admin.auth.email');
    }

    /*
    * Send Mail For Password Reset
     */
    public function sendEmail(Request $request)
    {
    	$credentials = ['email' => $request->input('email')];
    	$response = Password::sendResetLink($credentials, function (Message $message) {
    	    $message->subject($this->getEmailSubject());
    	});

    	switch ($response) {
    	    case Password::RESET_LINK_SENT:
    	        return redirect()->back()->with('status', trans($response));
    	    case Password::INVALID_USER:
    	        return redirect()->back()->withErrors(['email' => trans($response)]);
    	}
    }
}
