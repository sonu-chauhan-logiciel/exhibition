<?php

namespace App\Http\Controllers\Admin\Exhibition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ExhibitionStoreRequest;
use App\Repositories\ExhibitionRepository;

class ExhibitionController extends Controller
{
    protected $exhibition;

    public function __construct(ExhibitionRepository $exhibition)
    {
        $this->exhibition = $exhibition;
    }
    /**
     * Display a listing of the Exhibitions.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $exhibitions = $this->exhibition->paginate($request);
        return view('admin.exhibitions.exhibition', [
            'exhibitions' => $exhibitions 
        ]);
    }

    /**
     * Show the form for creating a new Exhibition.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.exhibitions.create');
    }

    /**
     * Store a newly created Exhibition in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExhibitionStoreRequest $request)
    {
        $this->exhibition->store($request);
        return redirect()->route('exhibitions.index')->with('message', 'Exhibition Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $exhibition = $this->exhibition->view($id);
        return view('admin.exhibitions.exhibitionView', [
            'exhibition' => $exhibition
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exhibition = $this->exhibition->find($id);
        return view('admin.exhibitions.edit', [
            'exhibition' => $exhibition
        ]);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ExhibitionStoreRequest $request, $id)
    {
        $this->exhibition->update($id, $request);
        return redirect()->route('exhibitions.index')->with('message', 'Exhibition Created Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->exhibition->delete($id);
        return redirect()->route('exhibitions.index')->with('message', 'Exhibition Deleted Successfully');
    }

    /**
     * Update the specified Exhibition Status in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request, $id)
    {
        $this->exhibition->statusUpdate($id, $request);
        return redirect()->route('exhibitions.index')->with('message', 'Exhibition Status Updated Successfully');
    }
}
