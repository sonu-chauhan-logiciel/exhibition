<?php

namespace App\Http\Controllers\Admin\Painting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PaintingRepository;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PaintingStoreRequest;
use App\Exhibition;

class PaintingController extends Controller
{
    protected $paintings;

    public function __construct(PaintingRepository $painting)
    {
        $this->paintings = $painting;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $request['user_id'] = Auth::id();
        $paintings = $this->paintings->paginate($request);
        return view('admin.paintings.painting', [
            'paintings' => $paintings
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing(Request $request)
    {   
        $paintings = $this->paintings->getPaginate($request);
        return view('admin.paintings.paintingListing', [
            'paintings' => $paintings
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $exhibitions = $this->paintings->getExhibition();
        return view('admin.paintings.create', [
            'exhibitions' => $exhibitions
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaintingStoreRequest $request)
    {
        $request['user_id'] = Auth::id();
        $this->paintings->store($request);
        return redirect()->route('paintings.index')->with('message', 'Painting Added Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $painting = $this->paintings->find($id);
        $exhibitions = $this->paintings->getExhibition();
        return view('admin.paintings.edit')->with(['painting' => $painting, 'exhibitions' => $exhibitions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PaintingStoreRequest $request, $id)
    {
        $this->paintings->update($id, $request);
        return redirect()->route('paintings.index')->with('message', 'Painting Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->paintings->delete($id);
        return redirect()->route('paintings.index')->with('message', 'Painting Deleted Successfully');
    }

    /**
     * Update the specified Painting Status in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request, $id)
    {
        $this->paintings->statusUpdate($id, $request);
        return redirect()->route('paintings.index')->with('message', 'User Status Updated Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paintings = $this->paintings->detail($id);
        return view('admin.paintings.detail')->with(['paintings' => $paintings]);
    }
}
