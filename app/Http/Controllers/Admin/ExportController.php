<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\UserRepository;

class ExportController extends Controller
{
	protected $user;

	public function __construct(UserRepository $user)
	{
	    $this->user = $user;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export(Request $request) 
    {
    	$users = $this->user->paginate($request);
        return Excel::download(new UsersExport($users), 'users.xlsx');
    }
}
