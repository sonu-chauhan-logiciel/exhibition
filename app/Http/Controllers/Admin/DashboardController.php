<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use DB;

class DashboardController extends Controller
{
    protected $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
    /*
    	Admin Dashboard View
     */
    public function adminDashboard()
    {
        $users = $this->users->getCount();
        $exhibitions = DB::table('exhibitions')->count();
        $paintings = $this->users->getPaintingCount();

    	return view('admin.dashboard')->with(['users' => $users, 'exhibitions' => $exhibitions, 'paintings' => $paintings]);
    }

    /*
    	User Dashboard View
     */
    public function userDashboard()
    {
        $paintings = $this->users->getPaintingCount();
        return view('admin.userDashboard', [
            'paintings' => $paintings
        ]);
    }
}	
