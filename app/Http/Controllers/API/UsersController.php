<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;
use League\Fractal\Serializer\JsonApiSerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Validator;

class UsersController extends Controller
{
	protected $user;

	public function __construct(UserRepository $user)
	{
	    $this->user = $user;
	}

	/**
	 * @return \IIluminate\Http\Response
	 */
	public function index(Request $request)
	{
        $paginator = $this->user->paginate($request);
        $users = $paginator->getCollection();
        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
        $resource = new Collection($users, new UserTransformer(), 'user');
        $result = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        return response()->json($manager->createData($result)->toArray());
	}

    /**
     * @param [int] $id
     * @return \IIluminate\Http\Response
     */
    public function show($id)
    {
    	$user = $this->user->show($id);
    	$manager = new Manager();
    	$manager->setSerializer(new JsonApiSerializer(url('api')));
    	$resource = new Item($user, new UserTransformer(), 'user');
    	$result = $manager->createData($resource);
    	return $result->toArray();
    }

    /**
     * @param [int] $id
     * @param \IIluminate\Http\Request $request
     * @return \IIluminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$this->user->update($id, $request);
    	$user = $this->user->show($id);
        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
        $resource = new Item($user, new UserTransformer(), 'user');
        $result = $manager->createData($resource)->toArray();
        return $result;
    }

    /**
     * @param [int] $id
     * @return \IIluminate\Http\Response
     */
    public function destroy($id)
    {
    	$this->user->delete($id);
    	return response()->json(['message' => 'User Deleted Successfully'],200);
    }
}
