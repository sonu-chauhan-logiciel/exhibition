<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ExhibitionRepository;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;
use League\Fractal\Serializer\JsonApiSerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformers\ExhibitionTransformer;
use Validator;


class ExhibitionController extends Controller
{
    protected $exhibition;

    public function __construct(ExhibitionRepository $exhibition)
    {
        $this->exhibition = $exhibition;
    }

    /**
     * @param \IIluminate\Http\Request $request
     * @return \IIluminate\Http\Response
     */
    public function index(Request $request)
    {
    	$paginator = $this->exhibition->paginate($request);
    	$exhibitions = $paginator->getCollection();
    	$manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
        $resource = new Collection($exhibitions, new ExhibitionTransformer(), 'exhibition');
    	$result = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
    	return response()->json($manager->createData($result)->toArray());
    }

    /**
     * Store Request at Storage
     * @param \IILuminate\Http\Request $request
     */
    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [ 
	        'name' => 'required',
            'start_at' => 'required|date|date_format:Y-m-d|before:end_at',
            'end_at' => 'required|date|date_format:Y-m-d|after:start_at',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'status' => 'accepted',
	    ]);
	    if ($validator->fails()) { 
	        return response()->json(['error'=>$validator->errors()], 401);            
	    }
	    $this->exhibition->store($request);
	    $exhibition = $this->exhibition->latestRecord();
        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
        $resource = new Item($exhibition, new ExhibitionTransformer(), 'Exhibition');
        $manager->parseIncludes('paintings');
        $result = $manager->createData($resource);
        return $result->toArray();
    }

    /**
     * Show Specific Exhibition
     * @param [int] $id
     * @return \IIluminate\Http\Response
     */
    public function show($id)
    {
    	$exhibition = $this->exhibition->view($id);
    	$manager = new Manager();
    	$manager->setSerializer(new JsonApiSerializer(url('api')));
    	$resource = new Item($exhibition, new ExhibitionTransformer(), 'Exhibition');
    	$manager->parseIncludes('paintings');
    	$result = $manager->createData($resource);
    	return $result->toArray();
    }

    /**
     * Update Specific Exhibition
     * @param [int] $id
     * @param \IIluminate\Http\Request $request
     * @return \IIluminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$this->exhibition->update($id, $request);
    	$exhibition = $this->exhibition->view($id);
        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
        $resource = new Item($exhibition, new ExhibitionTransformer(), 'Exhibition');
        $manager->parseIncludes('paintings');
        $result = $manager->createData($resource);
        return $result->toArray();
    }

    /**
     * Delete Specific Exhibition
     * @param [int] $id
     * @return \IIluminate\Http\Response
     */
    public function destroy($id)
    {
    	$this->exhibition->delete($id);
    	return response()->json(['message' => 'Exhibition Deleted Successfully'],200);
    }
}
