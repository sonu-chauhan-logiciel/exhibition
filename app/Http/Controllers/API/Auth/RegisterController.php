<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;
use League\Fractal\Serializer\JsonApiSerializer;
use Validator;


class RegisterController extends Controller
{
    protected $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * @param \IIluminate\Http\Request $request
     * @return \IIluminate\Http\Response
     */
    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required',
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
    	$this->user->store($request);
        $user = $this->user->latestRecord();
        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
        $resource = new Item($user, new UserTransformer(), 'user');
        $result = $manager->createData($resource)->toArray();
        return $result;
    }
}
