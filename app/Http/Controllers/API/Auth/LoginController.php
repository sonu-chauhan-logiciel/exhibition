<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    /**
     * Make User Login Request.
     *@param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1])){
            $user = User::where('email', $email)->first();
            if($user->role_id == 1){
	        	$tokenData = $user->createToken('Personal Accesss Token')->accessToken;
            }else{
	        	$tokenData = $user->createToken('Personal Accesss Token')->accessToken;
	        }
            return response()->json([
                'user' => $user,
                'token_type' => 'Bearer',
                'access_token' => $tokenData
            ], 200);
        }else{
        	return response()->json([
        		'message' => 'Invalid Credentials'
        	], 401);
        }
    }

    /**
     * @return \IIluminate\Http\Response $response
     */
    public function logout(Request $request)
    {	
        $user = $request->user();
        $user->token()->revoke();
        return Response(['code' => 200, 'message' => 'You are successfully logged out'], 200);
    }
}
