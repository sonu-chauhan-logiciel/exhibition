<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\PaintingRepository;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;
use League\Fractal\Serializer\JsonApiSerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformers\paintingTransformer;
use Validator;

class PaintingController extends Controller
{
    protected $painting;

    public function __construct(PaintingRepository $painting)
    {
        $this->painting = $painting;
    }

    /**
     * Get ALl Painting List
     * @return \IIluminate\Http\Response
     */
    public function index(Request $request)
    {
    	$paginator = $this->painting->getPaginate($request);
    	$paintings = $paginator->getCollection();
    	$manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
    	$resource = new Collection($paintings, new paintingTransformer());
    	$result = $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
    	return response()->json($manager->createData($result)->toArray());
    }

    /**
     * Store a newly created Painting in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [ 
	        'exhibition' => 'required',
	        'name' => 'required',
	        'image' => 'mimes:jpeg,jpg,png,gif|required',
            'status' => 'accepted',
	    ]);
	    if ($validator->fails()) { 
	        return response()->json(['error'=>$validator->errors()], 401);            
	    }
    	$user = $request->user();
    	$request['user_id'] = $user->id;
		$this->painting->store($request);
        $painting = $this->painting->latestRecord();
        $manager = new Manager();
        $manager->setSerializer(new JsonApiSerializer(url('api')));
        $resource = new Item($painting, new paintingTransformer(), 'Painting');
        $result = $manager->createData($resource)->toArray();
        return $result;
    }

    /**
     * Show Specific Painting
     * @return \IIluminate\Http\Response
     */
    public function show($id)
    {
		$painting = $this->painting->find($id);
    	$manager = new Manager();
    	$manager->setSerializer(new JsonApiSerializer(url('api')));
    	$resource = new Item($painting, new paintingTransformer(), 'Painting');
    	$result = $manager->createData($resource);
    	return $result->toArray();
    }

    /**
     * @param [int] $id
     * @param \IILuminate\Http\Request $request
     * @return \IILuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	
    	$this->painting->update($id, $request);
		$painting = $this->painting->find($id);
    	$manager = new Manager();
    	$manager->setSerializer(new JsonApiSerializer(url('api')));
    	$resource = new Item($painting, new paintingTransformer(), 'Painting');
    	$result = $manager->createData($resource)->toArray();
    	return $result;
    }

    /**
     * @param [int] $id
     * @return \IIluminate\Http\Response
     */
    public function destroy($id)
    {
    	$this->painting->delete($id);
    	return response()->json(['message' => 'Painting Deleted Successfully']);
    }
}
