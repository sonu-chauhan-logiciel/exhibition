<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Exhibition;

class Painting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exhibition_id', 'name', 'image', 'status',
    ];

    /**
     * Scope a query to search users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeFilter($query, $value)
    {
        $query->where('name', 'LIKE', "%$value%");
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function exhibition()
    {
        return $this->belongsTo('App\Exhibition');
    }
}
