<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PaintingPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any paintings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
       
    }

    /**
     * Determine whether the user can view the painting.
     *
     * @param  \App\User  $user
     * @param  \App\Painting  $painting
     * @return mixed
     */
    public function view(User $user)
    {
        //
    }

    /**
     * Determine whether the user can create paintings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if($user->role_id == 2){
            return true;   
        }else{
            return false;
        }
    }

    /**
     * Determine whether the user can update the painting.
     *
     * @param  \App\User  $user
     * @param  \App\Painting  $painting
     * @return mixed
     */
    public function update(User $user)
    {
        if($user->role_id == 2){
            return true;   
        }else{
            return false;
        }
    }

    /**
     * Determine whether the user can delete the painting.
     *
     * @param  \App\User  $user
     * @param  \App\Painting  $painting
     * @return mixed
     */
    public function delete(User $user)
    {
        if($user->role_id == 2){
            return true;   
        }else{
            return false;
        }
    }
}
