<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class UsersExport implements FromCollection, ShouldAutoSize, WithHeadings, WithTitle
{
	private $users;

    public function __construct($users) 
    {
        $this->users = $users;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	return $this->users;
    }

    public function headings(): array
	{
		return [
			'ID',
			'Name of User',
			'Email Id Of User',
			'Status',
		];
	}
      
    public function title(): string
	{
		return 'User Data';
	}
}
