<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Exhibition;

class ExhibitionTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'paintings'
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Exhibition $exhibition)
    {
        return [
            'id' => (int)$exhibition->id,
            'name' => $exhibition->name,
            'start_datetime' => $exhibition->start_datetime,
            'end_datetime' => $exhibition->end_datetime,
            'address' => $exhibition->address,
            'city' => $exhibition->city,
            'state' => $exhibition->state,
            'status' => $exhibition->status,
        ];
    }

    /**
     * Include Post
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includePaintings(Exhibition $exhibition)
    {
        $paintings = $exhibition->paintings;
        return $this->collection($paintings, new paintingTransformer, 'Paintings');
    }
}
