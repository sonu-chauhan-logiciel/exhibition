<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Painting;

class PaintingTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Painting $painting)
    {
        return [
            'id' => (int)$painting->id,
            'user_id' => (int)$painting->user_id,
            'exhibition_id' => (int)$painting->exhibition_id,
            'name' => $painting->name,
            'image' => $painting->image,
            'status' => $painting->status,
        ];
    }
}
