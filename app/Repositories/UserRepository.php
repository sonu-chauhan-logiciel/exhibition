<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;


class UserRepository 
{

    /**
     * Get ALl Users
     * @return \IIluminate\Http\Response
     */
    public function get()
    {
      $result = User::where('id', '>', 1)->get();
      return $result;  
    }

    /**
     * Get Users according Pagination
     * @param  \IIluminate\Http\Request $request
     * @return \IIluminate\Http\Response
     */
    public function paginate(Request $request)
    {
        $name = $request->input('name');
        $builder = new User;
        if ($name) {
            $builder = $builder->filter($name);   
        }
        $result = $builder->select('id','name','email','status')->paginate(4);
        return $result;
    }

    /**
     * Get Count of Users
     * @return \IIluminate\Http\Response
     */
    public function getCount()
    {
        $result = DB::table('users')->where('id', '>', 1)->count();
        return $result;
    }

    /**
     * Get Count of Paintings
     * @return \IIluminate\Http\Response
     */
    public function getPaintingCount(){
        $result = DB::table('paintings')->count();
        return $result;
    }

    /**
     * New User Store at Storage
     * @param  \IIluminate\Http\Request $request
     * @return \IIluminate\Http\Response
     */
    public function store(Request $request)
    {
        $password = Hash::make($request->input('password'));
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = $password;
        $result = $user->save();
        return $result;
    }

    /**
     * Show Specific User
     * @param  [int] $id
     * @return \IIluminate\Http\Response
     */
    public function show($id)
    {
        $result = User::findOrFail($id);
        return $result;  
    }

    /**
     * Find Specific User for Edit
     * @param  [int] $id
     * @return \IIluminate\Http\Response
     */
    public function find($id)
    {
        $result = User::findOrFail($id);
        return $result;
    }

    /**
     * Update Specific User
     * @param  [int] $id
     * @param  \IIluminate\Http\Request $request
     * @return \IIluminate\Http\Response
     */
    public function update($id, $request)
    {
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'status' => $request->input('status'),
        ];
        $result = User::where('id', $id)->update($data);
        /*$user = User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->status = $request->input('status');
        $result = $user->save();*/
        return $result;
    }

    /**
     * Delete Specific User 
     * @return \IIluminate\Http\Response
     */
    public function delete($id)
    {
        $user = User::find($id);
        $result = $user->delete();
        return $result;
    }

    /**
     * Get Email Id of Forget Password User
     * @return \IIluminate\Http\Response
     */
    public function getEmail($token){
        $records =  DB::table('password_resets')->get();
        foreach ($records as $record) {
            if (Hash::check($token, $record->token) ) {
               return $record->email;
            }
        }
    }

    /**
     * Update Specific User Status
     * @param  [int] $id
     * @param  \IIluminate\Http\Request $request
     * @return \IIluminate\Http\Response
     */
    public function statusUpdate($id, $request)
    {
        $user = User::find($id);
        $user->status = $request->input('status');
        $result = $user->save();
        return $result;
    }

    /**
     * Get Latest Record From Storage
     * @return \IIluminate\Http\Response
     */
    public function latestRecord()
    {
        $result = User::latest()->first();
        return $result;
    }
}
