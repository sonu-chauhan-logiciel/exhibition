<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Exhibition;
use App\Painting;

class ExhibitionRepository 
{
    /**
     * Get All Exhibitions
     */
    public function get()
    {
        $result = Exhibition::get();
        return $result;  
    }

    /**
     * Get Exhibitions Pccording Pagination
     */
    public function paginate(Request $request)
    {
        $name = $request->input('name');
        $builder = new Exhibition;
        if ($name) {
            $builder = $builder->filter($name);   
        }
        $result = $builder->paginate(4);
        return $result; 
    }

    /**
     * Store Exhibition Request to Storage
     */
    public function store(Request $request)
    {
        $exhibition = new Exhibition;
        $exhibition->name = $request->input('name');
        $exhibition->start_datetime = $request->input('start_at');
        $exhibition->end_datetime = $request->input('end_at');
        $exhibition->address = $request->input('address');
        $exhibition->city = $request->input('city');
        $exhibition->state = $request->input('state');
        $exhibition->status = $request->input('status');
        $result = $exhibition->save();
        return $result;
    }

    /**
     * Show Specific Exhibition
     * @param  [int] $id
     * @return \IIluminate\Http\Response
     */
    public function show($id)
    {
        $result = Exhibition::findOrFail($id);
        return $result;  
    }

    /*
    * Get Specific Exhibition View
     */
    public function view($id)
    {
        $result = Exhibition::with(['paintings'])->find($id);
        return $result;
    }

    /*
    *   Get Specific Exhibition For Edit
     */
    public function find($id)
    {
       $result = Exhibition::findOrFail($id);
       return $result; 
    }

    /*
    *   Update Specific Exhibition Data
     */
    public function update($id, $request)
    {
        $data = [
            'name' => $request->input('name'),
            'start_datetime' => $request->input('start_at'),
            'end_datetime' => $request->input('end_at'),
            'address' => $request->input('address'),
            'city' => $request->input('city'),
            'state' => $request->input('state'),
            'status' => $request->input('status')
        ];
        $result = Exhibition::where('id', $id)->update($data);
        return $result; 
    }

    /*
    *   Delete Specific Exhibition From Storage
     */
    public function delete($id)
    {
        $exhibition = Exhibition::find($id);
        $result = $exhibition->delete();
        return $result;
    }

    /*
    * Update Specific Exhibition Status
     */
    public function statusUpdate($id, $request)
    {
        $exhibition = Exhibition::find($id);
        $exhibition->status = $request->input('status');
        $result = $exhibition->save();
        return $result;
    }

    /**
     * Get Latest Record
     */
    public function latestRecord()
    {
        $result = Exhibition::latest()->first();
        return $result;
    }
}
