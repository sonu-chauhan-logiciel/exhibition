<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Painting;
use App\Exhibition;
use File;

class PaintingRepository 
{
    /**
     * Get All Paintings
     */
    public function getPaginate(Request $request)
    {
        $name = $request->input('name');
        $builder = new Painting;
        if ($name) {
            $builder = $builder->filter($name);   
        }
        $result = $builder->paginate(4);
        return $result;
    }

    /**
     * Get Paintings According Pagination
     */
    public function paginate(Request $request)
    {
        $name = $request->input('name');
        $id = $request->user_id;
        $builder = new Painting;
        if ($name) {
            $builder = $builder->where('name', 'LIKE', "%$name%");   
        }
        $result = $builder->where('user_id', $id)->paginate(4);
        return $result;
    }

    /*
    *   Get All Active Exhibitions
     */

    public function getExhibition()
    {
        $result = Exhibition::select('id','name')->where('status', 1)->get();
        return $result;
    }

    /**
     * Store Painting Request to Storage
     */
    public function store($request)
    {
        if ($request->hasfile('image')) {
            //image name with Extension
            $imagewithextension = $request->file('image')->getClientOriginalName();
            //image name without Extension
            $imagename = pathinfo($imagewithextension, PATHINFO_FILENAME);
            //get file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            $image = $imagename.'_'.uniqid().'.'.$extension;
            $request->file('image')->move(public_path().'/images/', $image);    
        }
        $painting = new Painting;
        $painting->user_id = $request->user_id;
        $painting->exhibition_id = $request->input('exhibition');
        $painting->name = $request->input('name');
        $painting->image = $image;
        $painting->status = $request->input('status');
        $result = $painting->save();
        return $result;
    }

    /*
    *   Get Specific Painting For Edit
     */
    public function find($id)
    {
        $result = Painting::findOrFail($id);
        return $result; 
    }

    /*
    *   Update Specific Painting Data
     */
    public function update($id, $request)
    {
        $data = [
            'exhibition_id' => $request->input('exhibition'),
            'name' => $request->input('name'),
            'status' => $request->input('status'),
        ];
        if ($request->hasfile('image')) {
            //image name with Extension
            $imagewithextension = $request->file('image')->getClientOriginalName();
            //image name without Extension
            $imagename = pathinfo($imagewithextension, PATHINFO_FILENAME);
            //get file extension
            $extension = $request->file('image')->getClientOriginalExtension();
            $image = $imagename.'_'.uniqid().'.'.$extension;
            $request->file('image')->move(public_path().'/images/', $image);  
            $data['image'] = $image;  
        }
        $result = Painting::where('id', $id)->update($data);
        return $result;
    }

    /*
    *   Delete Specific Painting From Storage
     */
    public function delete($id)
    {
        $painting = Painting::find($id);
        $image = $painting->image;
        $filename = public_path().'/images/'.$image;
        if (File::exists($filename)) {
            unlink($filename);
        }
        $result = $painting->delete();
        return $result;
    }

    public function search(Request $request)
    {
        $name = $request->input('name');
        $result = Painting::filter($name)->get();
        return $result;
    }

    /*
    * Update Specific Painting Status
     */
    public function statusUpdate($id, $request)
    {
        $painting = Painting::find($id);
        $painting->status = $request->input('status');
        $result = $painting->save();
        return $result;
    }

    /*
    * Deatil Of Specific Painting
     */
    public function detail($id)
    {
        $result = Painting::with(['user' => function($query) {
                    return $query->select(['id', 'name', 'email']);
                }, 'exhibition'])->find($id);
        return $result;
    }

    /**
     * Get Latest Record From Storage
     * @return \IIluminate\Http\Response
     */
    public function latestRecord()
    {
        $result = Painting::latest()->first();
        return $result;
    }
}
