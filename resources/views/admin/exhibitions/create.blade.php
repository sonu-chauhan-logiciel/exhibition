@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Admin Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!-- Users Listing-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			<div class="card o-hidden h-100">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4>
						Exhibition
					</h4>
				</div>
				<div class="card-body">
					<form action="{{ route('exhibitions.store') }}" method="POST">
						@csrf
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Name:<large class="text-danger">*</large></label>
									<input type="text" name="name" class="form-control" value="{{ old('name') }}">
									@error('name')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Start Date:<large class="text-danger">*</large></label>
									<input type="date" name="start_at" class="form-control" value="{{ old('start_at') }}">
									@error('start_at')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>End Date:<large class="text-danger">*</large></label>
									<input type="date" name="end_at" class="form-control" value="{{ old('end_at') }}">
									@error('end_at')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Address:<large class="text-danger">*</large></label>
									<textarea id="content" name="address" cols="30" rows="5" class="form-control">
										{{{ old('address') }}}
									</textarea>
									@error('address')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>City:<large class="text-danger">*</large></label>
									<input type="text" name="city" class="form-control" value="{{ old('city') }}">
									@error('city')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>State:<large class="text-danger">*</large></label>
									<input type="text" name="state" class="form-control" value="{{ old('state') }}">
									@error('state')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Status:</label>
									<input type="hidden" name="status" value="0">
									<input type="checkbox" name="status" value="1" class="mt-1" @if(old('status')=='1')) checked @endif>
									@error('status')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<input type="submit" name="exhibition" value="Add" class="btn btn-primary">
									<a href="{{ route('exhibitions.index') }}" class="btn btn-success">
										Back
									</a>
								</div>
							</div>
						</div>
					</form>	
				</div>
			</div>
		</div>
	</div>
@endsection