@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Admin Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!-- Exhibitions Listing-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			@if(session()->has('message'))
			    <div class="alert alert-success">
			        {{ session()->get('message') }}
			    </div>
			@endif
			<div class="card o-hidden h-100">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4>
						Exhibition Listing
					</h4>
					<form action="{{ route('exhibitions.index') }}" method="GET" role="search" class="d-flex">
						<input type="text" class="form-control" placeholder="Search" name="name" value="{{ Request::input('name') }}" required>
						<div class="input-group">
							&nbsp;
							<input type="submit" value="Search" class="btn btn-info">
							&nbsp;
							<a href="{{ route('exhibitions.index') }}" class="btn btn-success">Reset</a>
						</div>
					</form>
					<a href="{{ route('exhibitions.create') }}" class="btn btn-success">
						Add Exhibition
					</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">          
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Start Date/Time</th>
									<th>End Date/Time</th>
									<th>Address</th>
									<th>City</th>
									<th>State</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(count($exhibitions) > 0)
									@foreach($exhibitions as $exhibition)
										<tr>
											<td>{{$exhibition->id}}</td>
											<td>{{$exhibition->name}}</td>
											<td>{{date('d/m/Y', strtotime($exhibition->start_datetime))}}</td>
											<td>{{date('d/m/Y', strtotime($exhibition->end_datetime))}}</td>
											<td>{!!$exhibition->address!!}</td>
											<td>{{$exhibition->city}}</td>
											<td>{{$exhibition->state}}</td>
											<td>
												<form action="{{ route('exhibitions.status', ['id' => $exhibition->id]) }}" method="POST">
													{{ csrf_field() }}
													@if($exhibition->status == 1)
														<input type="hidden" name="status" value="0">
														<button type="submit" class="btn btn-success">
															<i class="fas fa-user-check" style="font-size: 10px;"></i>
														</button>
													@else
														<input type="hidden" name="status" value="1">
														<button type="submit" class="btn btn-danger">
															<i class="fas fa-window-close" style="font-size: 10px;"></i>
														</button>
													@endif
												</form>
											</td>
											<td class="d-flex">
												<a href="{{ route('exhibitions.show', ['id' => $exhibition->id]) }}" class="btn btn-info mr-1" style="height: 30px; width: 42px;">
													<i class="far fa-eye"></i>
												</a>
												<form method="POST" action="{{ route('exhibitions.delete', ['id' => $exhibition->id]) }}">
												    {{ csrf_field() }}
												    {{ method_field('DELETE') }}
												    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">
												    	<i class="fas fa-trash-alt"></i>
												    </button>
												</form>
												<a href="{{ route('exhibitions.edit', ['id' => $exhibition->id]) }}" class="btn btn-info ml-1" style="height: 30px; width: 42px;">
													<i class="far fa-edit"></i>
												</a>
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<th colspan="9">No Record's Found ! Try Again !!</th>
									</tr>
								@endif	
							</tbody>
						</table>
						{{ $exhibitions->links() }}
					</div>	
				</div>
			</div>
		</div>
	</div>
@endsection