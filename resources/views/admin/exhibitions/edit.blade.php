@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Admin Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!-- Users Listing-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			<div class="card o-hidden h-100">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4>
						Edit Exhibition
					</h4>
				</div>
				<div class="card-body">
					<form action="{{ route('exhibitions.update', ['id' => $exhibition->id]) }}" method="POST">
						@method('PUT')
						@csrf
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Name:</label>
									<input type="text" name="name" class="form-control" value="{{ $exhibition->name }}">
									@error('name')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Start Date:</label>
									<input class="form-control" type="date" name="start_at" placeholder="Start Date" value="{{Carbon\Carbon::parse($exhibition->start_datetime)->format('Y-m-d')}}">
									@error('start_at')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>End Date:</label>
									<input class="form-control" type="date" name="end_at" placeholder="End Date" value="{{Carbon\Carbon::parse($exhibition->end_datetime)->format('Y-m-d')}}">
									@error('end_at')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Address:</label>
									<textarea id="content" name="address" cols="30" rows="5" class="form-control">
										{{{ $exhibition->address }}}
									</textarea>
									@error('address')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>City:</label>
									<input type="text" name="city" class="form-control" value="{{ $exhibition->city }}">
									@error('city')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>State:</label>
									<input type="text" name="state" class="form-control" value="{{ $exhibition->state }}">
									@error('state')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Status:</label>
									<input type="hidden" name="status" value="0">
									<input type="checkbox" name="status" value="1" @if($exhibition->status == 1){{'checked'}}@endif>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<input type="submit" name="exhibition" value="Update" class="btn btn-primary">
									<a href="{{ route('exhibitions.index') }}" class="btn btn-success">
										Back
									</a>
								</div>
							</div>
						</div>
					</form>	
				</div>
			</div>
		</div>
	</div>
@endsection