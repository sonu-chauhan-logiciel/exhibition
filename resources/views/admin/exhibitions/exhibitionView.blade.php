@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Admin Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!-- Exhibitions Listing-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			@if(session()->has('message'))
			    <div class="alert alert-success">
			        {{ session()->get('message') }}
			    </div>
			@endif
			<div class="card o-hidden h-100">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4>
						Exhibition Detail
					</h4>
					<a href="{{ route('exhibitions.index') }}" class="btn btn-success">
						Back
					</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered">
							<tr>
								<th>Exhibition Name:</th>
								<td>{{$exhibition->name}}</td>
							</tr>
							<tr>
								<th>Start Date:</th>
								<td>{{date('d/m/Y', strtotime($exhibition->start_datetime))}}</td>
							</tr>
							<tr>
								<th>End Date:</th>
								<td>{{date('d/m/Y', strtotime($exhibition->end_datetime))}}</td>
							</tr>
							<tr>
								<th>Status:</th>
								<td>
									@if($exhibition->status == 1)
									<button type="button" class="btn btn-success">
										<i class="fas fa-user-check"></i>{{'Active'}}
									</button>
									@else
										<button type="button" class="btn btn-success">
											<i class="fas fa-close"></i>{{'Deactive'}}
										</button>
									@endif
								</td>
							</tr>
							<tr>
								<th>Venue:</th>
								<td>{!!$exhibition->address!!},&nbsp;{{$exhibition->city}},&nbsp;{{$exhibition->state}}</td>
							</tr>
							<tr>
								<th>Paintings:</th>
								<td>
									@foreach($exhibition->paintings as $painting)
										<table class="border" width="100%">
											<tr>
												<th>Painting Name:</th>
												<td>{{$painting->name}}</td>
											</tr>
											<tr>
												<th>Images:</th>
												<td>
													<img src="{{ asset('images/'.$painting->image) }}" alt="..." height="70px" width="100px">
												</td>
											</tr>
										</table>
									@endforeach
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection