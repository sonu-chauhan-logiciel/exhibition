@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Admin Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>
	<!-- Painting Listing-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			@if(session()->has('message'))
			    <div class="alert alert-success">
			        {{ session()->get('message') }}
			    </div>
			@endif

			<div class="card o-hidden h-100">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4>
						Painting Listing
					</h4>
					<form action="{{ route('paintings.listing') }}" method="GET" role="search" class="d-flex">
						<input type="text" class="form-control" placeholder="Search" name="name" value="{{ Request::input('name') }}" required>
						<div class="input-group">
							&nbsp;
							<input type="submit" value="Search" class="btn btn-info">
							&nbsp;
							<a href="{{ route('paintings.listing') }}" class="btn btn-success">Reset</a>
						</div>
					</form>
				</div>
				<div class="card-body">
					<div class="table-responsive">          
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Image</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(count($paintings) > 0)
									@foreach($paintings as $painting)
										<tr>
											<td>{{$painting->id}}</td>
											<td>{{$painting->name}}</td>
											<td>
												<img src="{{ asset('images/'.$painting->image)}}" alt="..." style="height: 50px; width: 50px;">
											</td>
											<td>
												<a href="{{ route('paintings.detail', ['id' => $painting->id]) }}" class="btn btn-info ml-1" style="height: 35px; width: 100px;">
													<i class="far fa-eye"></i>VIEW
												</a>
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="4">{{ 'No Record Found ! Try Again!!' }}</td>
									</tr>
								@endif
							</tbody>
						</table>
						{{ $paintings->links() }}
					</div>	
				</div>
			</div>
		</div>
	</div>
@endsection