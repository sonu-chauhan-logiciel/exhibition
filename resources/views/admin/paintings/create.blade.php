@extends('admin.layout.userMaster')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Artist Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!--Form Area-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			<div class="card o-hidden h-100">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4>
						Add Painting
					</h4>
					<a href="{{ route('paintings.index') }}" class="btn btn-success">
						Back
					</a>
				</div>
				<div class="card-body">
					<form action="{{ route('paintings.store') }}" method="POST" enctype="multipart/form-data">
						@csrf
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Exhibitions:</label>
									<select name="exhibition" class="form-control">
										<option value="">Select Exhibition</option>
										@foreach($exhibitions as $exhibition)
											<option value="{{$exhibition->id}}">{{$exhibition->name}}</option>
										@endforeach
									</select>
									@error('exhibition')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Name:</label>
									<input type="text" name="name" class="form-control" value="{{ old('name') }}">
									@error('name')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Image:</label>
									<input type="file" name="image" class="form-control">
									@error('image')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Status:</label>
									<input type="hidden" name="status" value="0">
									<input type="checkbox" name="status" value="1" class="mt-1" @if(old('status')=='1')) checked @endif>
									@error('status')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<input type="submit" name="Painting" value="Add" class="btn btn-primary">
								</div>
							</div>
						</div>
					</form>	
				</div>
			</div>
		</div>
	</div>
@endsection