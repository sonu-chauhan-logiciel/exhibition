@extends('admin.layout.userMaster')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Artist Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!-- Painting Listing-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			@if(session()->has('message'))
			    <div class="alert alert-success">
			        {{ session()->get('message') }}
			    </div>
			@endif

			<div class="card o-hidden h-100">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4>
						Painting Listing
					</h4>
					<form action="{{ route('paintings.index') }}" method="GET" role="search" class="d-flex">
						<input type="text" class="form-control" placeholder="Search" name="name" value="{{ Request::input('name') }}" required>
						<div class="input-group">
							&nbsp;
							<input type="submit" value="Search" class="btn btn-info">
							&nbsp;
							<a href="{{ route('paintings.index') }}" class="btn btn-success">Reset</a>
						</div>
					</form>
					<a href="{{ route('paintings.create') }}" class="btn btn-success">
						Add Painting
					</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">          
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Image</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(count($paintings) > 0)
									@foreach($paintings as $painting)
										<tr>
											<td>{{$painting->id}}</td>
											<td>{{$painting->name}}</td>
											<td>
												<img src="{{ asset('images/'.$painting->image)}}" alt="..." style="height: 50px; width: 50px;">
											</td>
											<td>
												<form action="{{ route('paintings.status', ['id' => $painting->id]) }}" method="POST">
													{{ csrf_field() }}
													@if($painting->status == 1)
														<input type="hidden" name="status" value="0">
														<button type="submit" class="btn btn-success">
															<i class="fas fa-user-check" style="font-size: 10px;"></i>
														</button>
													@else
														<input type="hidden" name="status" value="1">
														<button type="submit" class="btn btn-danger">
															<i class="fas fa-window-close" style="font-size: 10px;"></i>
														</button>
													@endif
												</form>
											</td>
											<td class="d-flex">
												<form method="POST" action="{{ route('paintings.delete', ['id' => $painting->id]) }}">
												    {{ csrf_field() }}
												    {{ method_field('DELETE') }}
												    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">
												    	<i class="fas fa-trash-alt"></i>
												    </button>
												</form>
												<a href="{{ route('paintings.edit', ['id' => $painting->id]) }}" class="btn btn-info ml-1" style="height: 30px; width: 42px;">
													<i class="far fa-edit"></i>
												</a>
											</td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="5">{{ 'No Record Found ! Try Again!!' }}</td>
									</tr>
								@endif
							</tbody>
						</table>
						{{ $paintings->links() }}
					</div>	
				</div>
			</div>
		</div>
	</div>
@endsection