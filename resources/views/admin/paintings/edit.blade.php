@extends('admin.layout.userMaster')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Artist Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!--Form Area-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			<div class="card o-hidden h-100">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4>
						Edit Painting
					</h4>
				</div>
				<div class="card-body">
					<form action="{{ route('paintings.update', ['id' => $painting->id]) }}" method="POST" enctype="multipart/form-data">
						@method('PUT')
						@csrf
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Exhibitions:</label>
									<select name="exhibition" class="form-control">
										<option value="">Select Exhibition</option>
										@foreach($exhibitions as $exhibition)
											<option value="{{$exhibition->id}}"
												@if($exhibition->id == $painting->exhibition_id)
													{{'Selected'}}
												@endif
											>
												{{$exhibition->name}}
											</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Name:</label>
									<input type="text" name="name" class="form-control" value="{{ $painting->name }}">
									@error('name')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<label>Image:</label>
									<input type="file" name="image" class="form-control">
									<img src="{{ asset('images/'.$painting->image) }}" alt="..." height="50px" width="50px">
									@error('image')
									    <div class="text-danger">{{ $message }}</div>
									@enderror
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<label>Status:</label>
								<input type="hidden" name="status" value="0">
								<input type="checkbox" name="status" value="1" @if($painting->status==1){{'checked'}}@endif>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-8">
								<div class="form-group">
									<input type="submit" name="Painting" value="Update" class="btn btn-primary">
									<a href="{{ route('paintings.index') }}" class="btn btn-success">
										Back
									</a>
								</div>
							</div>
						</div>
					</form>	
				</div>
			</div>
		</div>
	</div>
@endsection