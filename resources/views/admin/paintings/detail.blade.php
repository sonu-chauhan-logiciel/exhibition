@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Admin Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!-- Exhibitions Listing-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			@if(session()->has('message'))
			    <div class="alert alert-success">
			        {{ session()->get('message') }}
			    </div>
			@endif
			<div class="card o-hidden h-100">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4>
						Painting Detail
					</h4>
					<a href="{{ route('paintings.listing') }}" class="btn btn-success">
						Back
					</a>
				</div>
				<div class="card-body">	
					<div class="table-responsive">
						<table class="table table-bordered">
						    <tbody>
								<tr>
									<th>User Name:</th>
									<td>{{$paintings->user->name}}</td>
								</tr>
								<tr>
									<th>User Email:</th>
									<td>{{$paintings->user->email}}</td>
								</tr>
								<tr>
									<th>Exhibition Name:</th>
									<td>{{$paintings->exhibition->name}}</td>
								</tr>
								<tr>
									<th>Exhibition Start Date:</th>
									<td>{{date('d/m/Y', strtotime($paintings->exhibition->start_datetime))}}</td>
								</tr>
								<tr>
									<th>Exhibition End Date:</th>
									<td>{{date('d/m/Y', strtotime($paintings->exhibition->end_datetime))}}</td>
								</tr>
								<tr>
									<th>Exhibition Address:</th>
									<td>{!!$paintings->exhibition->address!!}</td>
								</tr>
								<tr>
									<th>Exhibition City:</th>
									<td>{{$paintings->exhibition->city}}</td>
								</tr>
								<tr>
									<th>Exhibition State:</th>
									<td>{{$paintings->exhibition->state}}</td>
								</tr>
								<tr>
									<th>Painting Name:</th>
									<td>{{$paintings->name}}</td>
								</tr>
								<tr>
									<th>Painting Image:</th>
									<td>
										<img src="{{ asset('images/'.$paintings->image) }}" alt="...">
									</td>
								</tr>
						    </tbody>
						  </table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection