@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Admin Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!-- Users Listing-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			<div class="card o-hidden h-100">
				<div class="card-header">
					<h4>
						Edit User
					</h4>
				</div>
				<div class="card-body">
					<div class="form">
						<form action="{{ route('users.update', ['id' => $user->id]) }}" method="POST">
							@method('PUT')
						    @csrf
						    <div class="row">
						        <div class="col-xl-6">
						            <div class="form-group">
						                <label for="">Name:</label>
						                <input type="text" name="name" class="form-control" value="{{ $user->name }}">
						                @error('name')
						                    <div class="text-danger">{{ $message }}</div>
						                @enderror
						            </div>
						        </div>
						    </div>
						    <div class="row">
						        <div class="col-xl-6">
						            <div class="form-group">
						                <label for="">Email:</label>
						                <input type="text" name="email" class="form-control" value="{{ $user->email }}">
						                @error('email')
						                    <div class="text-danger">{{ $message }}</div>
						                @enderror
						            </div>
						        </div>
						    </div>
						    <div class="row">
								<div class="col-xl-6">
									<div class="form-group">
										<label>Status:</label>
										<input type="hidden" name="status" value="0">
										<input type="checkbox" name="status" value="1" @if($user->status == 1){{'checked'}}@endif>
									</div>
								</div>
						    </div>
						    <div class="row">
						        <div class="col-xl-6">
						            <div class="form-group">
						                <input type="submit" name="submit" class="btn btn-primary" value="Update">
						                <a class="btn btn-success" href="{{ route('users.index') }}">Back</a>
						            </div>
						        </div>
						    </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection