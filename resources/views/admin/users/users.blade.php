@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Admin Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!-- Users Listing-->
	<div class="row">
		<div class="col-xl-12 col-sm-12 mb-12">
			@if(session()->has('message'))
			    <div class="alert alert-success">
			        {{ session()->get('message') }}
			    </div>
			@endif
			<div class="card o-hidden h-100">
				<div class="card-header d-flex align-items-center justify-content-between">
					<h4>
						User Listing
					</h4>
					<form action="{{ route('users.index') }}" method="GET" role="search" class="d-flex">
						<input type="text" class="form-control" placeholder="search" name="name" value="{{ Request::input('name') }}" required>
						<div class="input-group">
							&nbsp;
							<input type="submit" value="Search" class="btn btn-info">
							&nbsp;
							<a href="{{ route('users.index') }}" class="btn btn-success">Reset</a>
						</div>
					</form>
					<a class="btn btn-warning" href="{{ route('export', ['name'=> app('request')->input('name')]) }}">Export User Data</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">          
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Email</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@if(count($users) > 0)
									@foreach($users as $user)
										<tr>
											<td>{{ $user->id }}</td>
											<td>{{ $user->name }}</td>
											<td>{{ $user->email}}</td>
											<td>
												<form action="{{ route('users.status', ['id' => $user->id]) }}" method="POST">
													{{ csrf_field() }}
													@if($user->status == 1)
														<input type="hidden" name="status" value="0">
														<button type="submit" class="btn btn-success">
															<i class="fas fa-user-check" style="font-size: 10px"></i>
														</button>
													@else
														<input type="hidden" name="status" value="1">
														<button type="submit" class="btn btn-danger">
															<i class="fas fa-window-close" style="font-size: 10px"></i>
														</button>
													@endif
												</form>
											</td>
											<td class="d-flex">
												<form method="POST" action="{{ route('users.delete', ['id' => $user->id]) }}">
												    {{ csrf_field() }}
												    {{ method_field('DELETE') }}
												    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">
												    	<i class="fas fa-trash-alt"></i>
												    </button>
												</form>
												<a href="{{ route('users.edit', ['id' => $user->id]) }}" class="btn btn-info ml-1" style="height: 30px; width: 42px;">
													<i class="far fa-edit"></i>
												</a>
											</td>	
										</tr>
									@endforeach
								@else
									<tr>
										<th colspan="5">No Record's Found</th>
									</tr>
								@endif
							</tbody>
						</table>
						{{ $users->links() }}
					</div>	
				</div>
			</div>
		</div>
	</div>
@endsection