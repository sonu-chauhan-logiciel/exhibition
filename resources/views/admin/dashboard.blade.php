@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('content')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Admin Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	<!-- Icon Cards-->
	<div class="row">
		<div class="col-xl-4 col-sm-6 mb-3">
			<div class="card text-white bg-primary o-hidden h-100">
				<div class="card-body">
					<div class="card-body-icon">
						<i class="fas fa-fw fa-comments"></i>
					</div>
					<div class="mr-5">{{ $users }} Users</div>
				</div>
				<a class="card-footer text-white clearfix small z-1" href="{{ route('users.index') }}">
					<span class="float-left">View Users</span>
					<span class="float-right">
						<i class="fas fa-angle-right"></i>
					</span>
				</a>
			</div>
		</div>
		<div class="col-xl-4 col-sm-6 mb-3">
			<div class="card text-white bg-warning o-hidden h-100">
				<div class="card-body">
					<div class="card-body-icon">
						<i class="fas fa-fw fa-list"></i>
					</div>
					<div class="mr-5">{{ $exhibitions }} Exhibitions!</div>
				</div>
				<a class="card-footer text-white clearfix small z-1" href="{{ route('exhibitions.index') }}">
					<span class="float-left">View Details</span>
					<span class="float-right">
						<i class="fas fa-angle-right"></i>
					</span>
				</a>
			</div>
		</div>
		<div class="col-xl-4 col-sm-6 mb-3">
			<div class="card text-white bg-success o-hidden h-100">
				<div class="card-body">
					<div class="card-body-icon">
						<i class="fas fa-fw fa-shopping-cart"></i>
					</div>
					<div class="mr-5">{{$paintings}} Paintings!</div>
				</div>
				<a class="card-footer text-white clearfix small z-1" href="{{ route('paintings.listing') }}">
					<span class="float-left">View Details</span>
					<span class="float-right">
					<i class="fas fa-angle-right"></i>
					</span>
				</a>
			</div>
		</div>
	</div>
@endsection