<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::group(["middleware" => "auth", "namespace" => "Admin"], function(){
	Route::get('/dashboard', 'DashboardController@adminDashboard')->name('admindashboard');
	Route::get('/userdashboard', 'DashboardController@userDashboard')->name('userdashboard');
});*/

Route::namespace('Admin\Login')->group(function(){
	Route::get('/login', 'LoginController@showForm')->name('login');
	Route::post('/login', 'LoginController@login');
	Route::post('/logout', 'LoginController@logout')->name('logout');
});

Route::group(["middleware" => ["auth","preventBackHistory"], "namespace" => "Admin"], function(){
	Route::get('/dashboard', 'DashboardController@adminDashboard')->name('admindashboard');
	Route::get('/userdashboard', 'DashboardController@userDashboard')->name('userdashboard');
});

Route::namespace('Admin\Auth')->group(function(){
	Route::get('/login', 'LoginController@showForm')->name('login');
	Route::post('/login', 'LoginController@login');
	Route::post('/logout', 'LoginController@logout')->name('logout');
	Route::get('/passwordReset', 'ForgetPasswordController@showForm')->name('password.request');
	Route::post('/password/email', 'ForgetPasswordController@sendEmail')->name('password.email');
	Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
	Route::post('password/reset', 'ResetPasswordController@resetPassword')->name('password.update');
});

Route::namespace('Admin\User')->group(function(){
	Route::prefix('users')->name('users.')->group(function(){
		Route::get('/', 'UsersController@index')->name('index');
		Route::get('/register', 'UsersController@create')->name('create');
		Route::post('/', 'UsersController@store')->name('store');
		Route::get('/edit/{id}', 'UsersController@edit')->name('edit');
		Route::put('/{id}', 'UsersController@update')->name('update');
		Route::delete('/delete/{id}', 'UsersController@destroy')->name('delete');
		Route::post('/{id}', 'UsersController@status')->name('status');
	});
});

Route::namespace('Admin\Exhibition')->group(function(){
	Route::prefix('exhibitions')->name('exhibitions.')->group(function(){
		Route::get('/', 'ExhibitionController@index')->name('index');
		Route::get('/create', 'ExhibitionController@create')->name('create');
		Route::post('/', 'ExhibitionController@store')->name('store');
		Route::get('view/{id}', 'ExhibitionController@show')->name('show');
		Route::get('/edit/{id}', 'ExhibitionController@edit')->name('edit');
		Route::put('/{id}', 'ExhibitionController@update')->name('update');
		Route::delete('/delete/{id}', 'ExhibitionController@destroy')->name('delete');
		Route::post('/{id}', 'ExhibitionController@status')->name('status');
	});
});

Route::namespace('Admin\Painting')->group(function(){
	Route::prefix('paintings')->name('paintings.')->group(function(){
		Route::get('/', 'PaintingController@index')->name('index');
		Route::get('/create', 'PaintingController@create')->name('create');
		Route::post('/', 'PaintingController@store')->name('store');
		Route::get('/edit/{id}', 'PaintingController@edit')->name('edit');
		Route::put('/{id}', 'PaintingController@update')->name('update');
		Route::delete('/delete/{id}', 'PaintingController@destroy')->name('delete');
		Route::post('/{id}', 'PaintingController@status')->name('status');
		Route::get('listing', 'PaintingController@listing')->name('listing');
		Route::get('detail/{id}', 'PaintingController@show')->name('detail');
	});
});

Route::namespace('Admin')->group(function (){
	Route::get('export', 'ExportController@export')->name('export');
});

/*Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');*/
