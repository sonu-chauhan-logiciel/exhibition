<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

/*Route::post('/login', 'API\LoginController@login');
Route::get('/logout', 'API\LoginController@logout');*/

Route::namespace('API\Auth')->group(function(){
	Route::post('/login', 'LoginController@login');
	Route::post('/register', 'RegisterController@store');	
});

/*Route::group(['middleware' => 'auth:api'], function(){
	Route::namespace('API')->group(function(){
		Route::prefix('users')->group(function(){
			Route::get('/', 'UsersController@index');
			Route::get('/{id}', 'UsersController@show');
			Route::put('/update/{id}', 'UsersController@update');
			Route::delete('/delete/{id}', 'UsersController@destroy');
		});
		Route::prefix('exhibitions')->group(function(){
			Route::get('/', 'ExhibitionController@index');
			Route::post('/create', 'ExhibitionController@store');
			Route::get('/{id}', 'ExhibitionController@show');
			Route::put('/update/{id}', 'ExhibitionController@update');
			Route::delete('/delete/{id}', 'ExhibitionController@destroy');
		});
		Route::prefix('paintings')->group(function(){
			Route::get('/', 'PaintingController@index');
			Route::post('/create', 'PaintingController@store');
			Route::get('/show/{id}', 'PaintingController@show');
			Route::put('/update/{id}', 'PaintingController@update');
			Route::delete('/delete/{id}', 'PaintingController@destroy');
		});
	});
	Route::namespace('API\Auth')->group(function(){
		Route::get('/logout', 'LoginController@logout');	
	});
});*/

Route::namespace('API')->group(function(){
	Route::middleware(['auth:api'])->group(function(){
		Route::prefix('users')->group(function(){
			Route::get('/', 'UsersController@index');
			Route::get('/{id}', 'UsersController@show');
			Route::put('/update/{id}', 'UsersController@update');
			Route::delete('/delete/{id}', 'UsersController@destroy');
		});
		Route::prefix('exhibitions')->group(function(){
			Route::get('/', 'ExhibitionController@index');
			Route::post('/create', 'ExhibitionController@store');
			Route::get('/{id}', 'ExhibitionController@show');
			Route::put('/update/{id}', 'ExhibitionController@update');
			Route::delete('/delete/{id}', 'ExhibitionController@destroy');
		});
	});
	Route::middleware(['auth:api'])->group(function(){
		Route::prefix('paintings')->group(function(){
			Route::get('/', 'PaintingController@index');
			Route::post('/create', 'PaintingController@store')->middleware('can:update,App\Painting');
			Route::get('/show/{id}', 'PaintingController@show')->middleware('can:update,App\Painting');
			Route::put('/update/{id}', 'PaintingController@update')->middleware('can:update,App\Painting');
			Route::delete('/delete/{id}', 'PaintingController@destroy')->middleware('can:update,App\Painting');
		});
	});
});
Route::namespace('API\Auth')->group(function(){
	Route::middleware(['auth:api'])->group(function(){
		Route::get('/logout', 'LoginController@logout');
	});	
});


